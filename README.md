# XFCE_2020

Fedora XFCE 2020 dotfiles

![Preview](Fedora_2020-09-08_20-22-26.png "Fedora 32 XFCE / Rphl")

## Repository
Enable repository RPM Fusion Free for additionnal packages:

`sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm`

## Packages
dnf install
*    bashtop
*    bmon
*    conky
*    cmus
*    exfalso
*    git
*    google-roboto-fonts
*    googler
*    gpick
*    grsync
*    materia-gtk-theme
*    numlockx
*    neofetch
*    papirus-icon-theme
*    ranger
*    rofi
*    xed
*    zsh

`sudo dnf install bashtop bmon cmus conky exfalso git google-roboto-fonts googler gpick grsync materia-gtk-theme neofetch numlockx papirus-icon-theme ranger rofi xed zsh`

## Theming
- Fonts: https://www.nerdfonts.com/ - JetBrains used in dotfiles
- Fonts alternative: Iosevka https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Iosevka 
- Sounds: extract /Borealis in ~l/.local/share/sounds/, enable events & system sounds in 'Appearance', set 'Borealis' as your theme in Settings Editor>xsettings>SoundThemeName

## Settings
XFCE4-panel {
*    Workspaces 1-3
*    Launcher: standard app menu (just in case)
*    Button Windows
*    {separator}
*    3 xfce4-genmon-scripts (source the script in the applet 'sh ~/bin/script.sh')
*    Orage calendar: date
*    Launcher: script for VPN session
*    Notification area 
*    Audio
*    Battery
*    Notifications
*    launcher: xfce4-session-logout
*    Orage calendar: hour
}

## Desktop
- Rofi launcher: displayed over top-panel
- Conky: application shortcuts (off on screenshot)
